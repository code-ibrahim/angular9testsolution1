import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  
  constructor(private http: HttpClient) { }

  getImages(): Observable<string> {
    console.log("getImages in service");
    let temVar = this.http.get<string>('https://localhost:5001/api/Image').pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  errorHandler(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error.message || "Server Error");
  } 


}
