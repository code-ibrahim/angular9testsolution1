import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ImagesService } from '../../angulartest-services/images.service';

@Component({
  selector: 'app-test-component1',
  templateUrl: './test-component1.component.html',
  styleUrls: ['./test-component1.component.css']
})
export class TestComponent1Component implements OnInit {

  constructor(private _imageService: ImagesService) { }

  List1: string[] = ["abc", "def"];
  List2: string[] = ["i", "j", "k"];
  imageNameList: string[] = ["i", "j", "k"];
  imgObj: string;

  ngOnInit(): void {
  }

  async getImageeNames() {
    console.log("getImageeNames clicked");
    this.imgObj = undefined;
    this._imageService.getImages().subscribe(
      r => {
        this.imgObj = r;
        console.log("responce : " + r);
      },
      er => {
        this.imgObj = "error";
        console.log(er);
      },
      () => console.log("getImageeNames executed")
    );
    let count = 0;
    while (this.imgObj == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("imgObj : " + this.imgObj);
    if (this.imgObj != "error") {
      this.imageNameList = this.imgObj.split("\n");
    }

  }


  showList1() {
    console.log("showList1 clicked")
    this.List1 = ["1", "2", "3"];
  }

  showList2(item) {
    console.log("showList2 clicked")
    if (item == "i") {
      this.List1 = ["a", "b", "c"];
    }
    else if (item == "j") {
      this.List1 = ["p", "q", "r"];
    }
    else if (item == "k") {
      this.List1 = ["l", "m", "n"];
    }
    else {
      this.List1 = ["r", "s", "t"];
    }
    
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  login() {
    console.log("inside login");
    sessionStorage.setItem('userName', "abc@def.com");
  }

  logout() {
    console.log("inside logout");
    sessionStorage.removeItem('userName');
  }

}
