import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { TestComponent1Component } from './test-component1/test-component1.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  //{ path: '', component: homeComponent },
  { path: 'testComponent1', component: TestComponent1Component } //,
  //{ path: '**', component: TestComponent1Component }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
