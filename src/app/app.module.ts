import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TestComponent1Component } from './test-component1/test-component1.component';
import { FormsModule, NgForm } from '@angular/forms';
import { ImagesService } from '../angulartest-services/images.service';
import { HttpClientModule } from '@angular/common/http';
import { routing } from './app.routing';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent1Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [ImagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
